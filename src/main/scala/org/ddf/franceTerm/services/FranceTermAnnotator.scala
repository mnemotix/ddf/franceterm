/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services

import akka.Done
import com.typesafe.scalalogging.LazyLogging
import org.ddf.annotator.services.{DDFDictionnaireAnnotator, DDFDictionnaireSenseRelationAnnotator}
import org.ddf.franceTerm.helper.{FranceTermConfig, FranceTermeMapperException}

import java.io.File
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

class FranceTermAnnotator extends LazyLogging{

  implicit val executionContext = ExecutionContext.global
  lazy val dirName = FranceTermConfig.dir


  def franceTermAnnotate = {
    val path = new File(s"${dirName}output/franceTerme.csv")
    val annotator = new DDFDictionnaireAnnotator(path.getAbsolutePath, dicoName = Some("franceterme"),
      dictionnaireUriPrefix = Some("http://data.dictionnairedesfrancophones.org/dict/franceterme"),
      uriDictionnaire = Some("http://data.dictionnairedesfrancophones.org/resource/franceterme"),
      graphUri = Some("http://data.dictionnairedesfrancophones.org/dict/franceterme/graph"))
    val fut: (Future[Done], Future[Done]) = annotator.annotator
    val result: Future[Seq[Done]] = Future.sequence(Seq(fut._1, fut._2))
    annotator.init

    result.onComplete {
      case Success(value) => {
        println(s"first passage is finished with value = $value")
        annotator.shutdown
      }
      case Failure(e) => {
        logger.error(s"an error occured during the mapping of FranceTerme: $e")
        throw FranceTermeMapperException("an error occured during the mapping of FranceTerme", Some(e))
      }
    }
    val finalResult = Await.result(result, Duration.Inf)
    annotator.shutdown

    val annotator2 = new DDFDictionnaireSenseRelationAnnotator(path.getAbsolutePath, dicoName = Some("franceterme"),
      dictionnaireUriPrefix = Some("http://data.dictionnairedesfrancophones.org/dict/franceterme"),
      uriDictionnaire = Some("http://data.dictionnairedesfrancophones.org/resource/franceterme"),
      graphUri = Some("http://data.dictionnairedesfrancophones.org/dict/franceterme/graph"))
    val future2 = annotator2.annotate
    future2.onComplete {
      case Success(value) => {
        println(s"second passage is finished with value = $value")
        annotator2.shutdown
      }
      case Failure(e) => {
        logger.error(s"an error occured during the mapping of sense relation of FranceTerme: $e")
        throw FranceTermeMapperException("an error occured during the mapping of sense relation of FranceTerme", Some(e))
      }

    }
    val finalResult2 = Await.result(future2, Duration.Inf)
  }

  def annotateSr = {
    val path = new File(s"${dirName}output/franceTerme.csv")
    val annotator2 = new DDFDictionnaireSenseRelationAnnotator(path.getAbsolutePath)
    val future2 = annotator2.annotate
    future2.onComplete {
      case Success(value) => {
        println(s"second passage is finished with value = $value")
        annotator2.shutdown
      }
      case Failure(e) => {
        throw FranceTermeMapperException("an error occured during the mapping of FranceTerme", Some(e))

      }
    }
    val finalResult2 = Await.result(future2, Duration.Inf)
  }
}