import sbt._

object Version {
  lazy val akkaActor = "2.5.23"
  lazy val alpakkaXml = "3.0.4"
  lazy val config = "1.3.3"
  lazy val commons = "2.6"
  lazy val commonsCodec = "1.10"
  lazy val commonsHTTPClient = "3.1"
  lazy val htmlCleaner = "2.22"
  //lazy val lifty = "0.1.2-SNAPSHOT"
  lazy val logback = "1.2.3"
  lazy val play = "2.7.0-RC9"
  lazy val playJson = "2.7.0-M1"
  lazy val playWSStandolone = "2.0.0-RC2"
  lazy val rdf4j = "2.5.3"
  lazy val rocksdb = "6.2.2"
  lazy val scalaLangMod = "2.1.0"
  lazy val scalaLogging = "3.9.0"
  lazy val scalaTest = "3.2.9"
  lazy val synaptixVersion = "3.1.2-SNAPSHOT"
  lazy val scalaVersion = "2.13.8"
  lazy val openCsv = "5.4"
}

object Library {
  lazy val alpakkaXml = "com.lightbend.akka" %% "akka-stream-alpakka-xml" % Version.alpakkaXml
  lazy val config: ModuleID = "com.typesafe" % "config" % Version.config
  lazy val commons = "commons-io" % "commons-io" % Version.commons
  lazy val commonsCodec = "commons-codec" % "commons-codec" % Version.commonsCodec
  lazy val commonsHTTPClient = "commons-httpclient" % "commons-httpclient" % Version.commonsHTTPClient
  lazy val htmlCleaner = "net.sourceforge.htmlcleaner" % "htmlcleaner" % Version.htmlCleaner
  //lazy val lifty = "com.mnemotix" %% "analytix-lifty" % Version.synaptixVersion
  lazy val logbackClassic: ModuleID = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val scalaLangMod = "org.scala-lang.modules" %% "scala-xml" % Version.scalaLangMod
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val scalaTest: ModuleID = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val synaptixRdfToolKit = "com.mnemotix" %% "synaptix-rdf-toolkit" % Version.synaptixVersion
  lazy val synaptixCore = "com.mnemotix" %% "synaptix-core" % Version.synaptixVersion
  lazy val synaptixHtmlToolkit = "com.mnemotix" %% "synaptix-http-toolkit" % Version.synaptixVersion
  lazy val synaptixCacheToolkit = "com.mnemotix" %% "synaptix-cache-toolkit" % Version.synaptixVersion

  lazy val openCsv = "com.opencsv" % "opencsv" % Version.openCsv
  lazy val ddfAnnotator = "org.ddf" %% "ddf-annotator" % "1.0.2-SNAPSHOT"
  lazy val quartz = "org.quartz-scheduler" % "quartz" % "2.3.1"

}

object Dependencies {

  import Library._

  val conf = List(config)
  val common = List(commons, commonsCodec, commonsHTTPClient)
  val log = List(logbackClassic)
  val scala = List(scalaLangMod)
  val synaptix = List(synaptixCore, synaptixHtmlToolkit, synaptixCacheToolkit, alpakkaXml, openCsv, ddfAnnotator, synaptixRdfToolKit)
  val jobQuartz = List(quartz)
  val test = List(scalaTest)

  val compileScope = conf  ++ scala ++ common ++ synaptix ++ log ++ jobQuartz

  val testScope = (test).map(_ % "test")

  // Projects
  val core = compileScope ++ testScope
}