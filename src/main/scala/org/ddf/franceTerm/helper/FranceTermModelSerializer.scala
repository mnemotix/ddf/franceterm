/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.helper

import org.ddf.franceTerm.model.{Antonyme, ArticleProperty, Capitale, Domaine, Equivalent, Synonyme, Terme, Toponyme, Variante, Variantes, Voir}

import scala.collection.immutable

object FranceTermModelSerializer {

  def articleProperty(elem: scala.xml.Node): ArticleProperty = {
    val numero = if (elem.attribute("numero").isDefined) Some(elem.attribute("numero").get(0).text) else None
    val commission = if (elem.attribute("commission").isDefined) Some(elem.attribute("commission").get(0).text) else None
    val id = if (elem.attribute("id").isDefined)  Some(elem.attribute("id").get(0).text) else None
    val publie = if (elem.attribute("publie").isDefined) Some(elem.attribute("publie").get(0).text) else None
    val annule = if (elem.attribute("annule").isDefined) Some(elem.attribute("annule").get(0).text) else None
    val administration = if (elem.attribute("administration").isDefined) Some(elem.attribute("administration").get(0).text) else None
    ArticleProperty(numero, commission, id, publie, annule, administration)
  }

  def terme(elem: scala.xml.Node): Terme = {
    val terme = (elem \ "Terme")
    val n = terme.filter(_.attribute("statut").isDefined).
      filter(_.\@("statut").trim == "privilegie")
    if (n.size> 0) {
      n.map { node =>
        val categorie = if (node.attribute("categorie").isDefined) Some(node.\@("categorie")) else None
        val termeBalise = if (node.attribute("Terme").isDefined) Some(node.\@("Terme")) else None
        Terme(None, categorie, None, termeBalise)
      }.last
    }
    else Terme(None, None, None, None)
  }

  def synonyme(elem: scala.xml.Node) = {
    val terme = (elem \ "Terme")

    terme.filter(_.attribute("statut").isDefined).filter(_.\@("statut") == "synonyme").map { node =>
      val status = if (node.attribute("status").isDefined) Some(node.\@("status")) else None
      val termeBalise = if (node.text.trim != "") Some(node.text.trim) else None
      val terme = if (node.\@("Terme").trim != "" ) Some(node.\@("Terme").trim) else None
      Synonyme(status, terme)
    }
  }

  def antonyme(elem: scala.xml.Node) = {
    val terme = (elem \ "Terme")

    terme.filter(_.attribute("statut").isDefined).filter(_.\@("statut") == "antonyme").map { node =>
      val status = if (node.attribute("status").isDefined) Some(node.\@("status")) else None
      val termeBalise = if (node.text.trim != "") {
        if (node.text.trim.contains(",")) None else Some(node.text.trim)
      }  else None
      Antonyme(status, termeBalise)
    }
  }

  def toponyme(elem: scala.xml.Node) = {
    val terme = (elem \ "Terme")

    terme.filter(_.attribute("statut").isDefined).filter(_.\@("statut") == "Toponyme").map { node =>

      val status = if (node.attribute("status").isDefined) Some(node.\@("status")) else None
      val termeBalise = if (node.text.trim != "") {
        if (node.text.trim.contains(",")) None else Some(node.text.trim)
      }  else None
      Toponyme(status, termeBalise)
    }
  }

  def capitale(elem: scala.xml.Node) = {
    val terme = (elem \ "Terme")

    terme.filter(_.attribute("statut").isDefined).filter(_.\@("statut") == "Capitale").map { node =>
      val status = if (node.attribute("status").isDefined) Some(node.\@("status")) else None
      val termeBalise = if (node.text.trim != "") Some(node.text.trim) else None
      Capitale(status, termeBalise)
    }
  }

  def variante(elem: scala.xml.Node): Variantes = {
    val terme = (elem \ "Terme")
    val vs = terme.filter(_.attribute("statut").isDefined).filter(_.\@("statut") == "privilegie").map { node =>
      val variantes = (node \ "variantes")
      val v: immutable.Seq[Variante] = variantes.filter(_.attribute("type").isDefined).filter(_.\@("type") == "Abréviation").map { abrev =>
        Variante(Some((abrev \ "variante").text.trim), None, Some("Abréviation"), None)
      }
      v
    }
    Variantes(vs.flatten, None,  Some("Abréviation"), None)
  }

  /*
case class Variante(variante_text: Option[String], lang: Option[String], `type`: Option[String], origine: Option[String])

case class Variantes(variante: Seq[Variante], lang: Option[String], `type`: Option[String], origine: Option[String])

<Terme statut="privilegie" Terme="matrice de micromiroirs">
<variantes type="Abréviation">
<variante type="Abréviation">
MDM</variante></variantes>
matrice de micromiroirs</Terme>
 */

  def termeBalise(elem: scala.xml.Node) = {
    if ((elem \ "Terme_balise").text.trim != "") Some((elem \ "Terme_balise").text.trim) else None
  }

  def termeExergue(elem: scala.xml.Node) = {
    if ((elem \ "Terme_exergue" \ "strong").text.trim != "") Some((elem \ "Terme_exergue" \ "strong").text.trim) else None
  }

  def datePub(elem: scala.xml.Node) = {
    if ((elem \ "DatePub").text.trim != "") Some((elem \ "DatePub").text.trim) else None
  }

  def note(elem: scala.xml.Node) = {
    if ((elem \ "Notes").text.trim != "") Some((elem \ "Notes").text.trim) else None
  }

  def definition(elem: scala.xml.Node) = {
    if ((elem \ "Definition").text.trim != "") Some((elem \ "Definition").text.trim) else None
  }

  def source(elem: scala.xml.Node) = {
    if ((elem \ "Source").text.trim != "") Some((elem \ "Source").text.trim) else None
  }

  def equivalent(elem: scala.xml.Node): Option[Seq[Equivalent]] = None

  def domaine(elem: scala.xml.Node) = {
    val domaineNode = (elem \ "Domaine")

    if (domaineNode.size > 0) {
      val doms = domaineNode \ "Dom"
      val sDoms = domaineNode \ "S-dom"

      val domsString = if (doms.size > 0) {
        Some(doms.map(_.text).toList)
      }
      else None

      val sDomsString = if (sDoms.size > 0) {
        Some(sDoms.map(_.text).toList)
      }
      else None

      Some(Domaine(domsString, sDomsString))
    }
    else None
  }

  def voir(elem: scala.xml.Node) = {
    val vNodes = (elem \ "Voir" \ "A")

    val s = vNodes.map { v =>
      Voir(v.text)
    }
    if (vNodes.size > 0) Some(s) else None
  }
}