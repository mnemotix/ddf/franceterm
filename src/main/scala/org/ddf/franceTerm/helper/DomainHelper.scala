/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.helper
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.IOResult
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}

import java.nio.file.Paths
import akka.stream.scaladsl.{FileIO, Flow, Sink, Source}
import com.mnemotix.synaptix.core.SynaptixException
import org.ddf.franceTerm.model.FranceTermeDomaineDDF

import java.nio.charset.StandardCharsets
import scala.collection.immutable
import scala.concurrent.{ExecutionContext, Future}

class DomainHelper(implicit ec: ExecutionContext, system: ActorSystem) {

  def mapValue(aMap: Map[String, String], key: String): Option[String] = if (aMap.get(key).isDefined && !aMap.get(key).get.trim.isEmpty && aMap.get(key).get != " "  && aMap.get(key).get != "") Some(aMap.get(key).get.trim) else None

  def extract: Source[Map[String, String], Future[IOResult]] = {
    try {
      FileIO.fromPath(Paths.get(s"src/main/resources/FranceTerme_domain_DDF.csv"))
        .via(CsvParsing.lineScanner(maximumLineLength = 100 * 1024))
        .via(CsvToMap.toMapAsStrings(StandardCharsets.UTF_8))
    }
    catch {
      case t: Throwable => {
        throw FranceTermeExtractionException("An error occured during the resource extraction process", Some(t))
      }
    }
  }

  def setter:Flow[Map[String, String], FranceTermeDomaineDDF, NotUsed] = {
    Flow[Map[String, String]].mapAsync(500) { aMap =>
      val prefLabel = aMap.get("skos:prefLabel@fr").get.trim.toLowerCase
      val uri = aMap.get("URI").get.trim
      val uri2 = aMap.get("URI2").get.trim
      Future(FranceTermeDomaineDDF(prefLabel, uri, uri2))
    }
  }

  def domainHelper: Future[immutable.Seq[FranceTermeDomaineDDF]] = {
    extract.via(setter).runWith(Sink.seq)
  }
}