import sbt.url
import com.gilcloud.sbt.gitlab.GitlabPlugin

enablePlugins(sbtdocker.DockerPlugin, sbtassembly.AssemblyPlugin)

val meta = """META.INF(.)*""".r
ThisBuild / scalaVersion := Version.scalaVersion
/*ThisBuild / version := Version.synaptixVersion*/
ThisBuild / organization := "com.mnemotix"
ThisBuild / organizationName := "MNEMOTIX SCIC"

lazy val franceTerm = (project in  file(".")).
  settings(
    name := "franceTerm",
    organization := "org.ddf",
    scalaVersion := Version.scalaVersion,
    version := "1.0.0-SNAPSHOT",
    licenses := Seq("Apache 2.0" -> url("https://opensource.org/licenses/Apache-2.0")),
    developers := List(
      Developer(
        id = "prlherisson",
        name = "Pierre-René Lhérison",
        email = "pr.lherisson@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
      Developer(
        id = "ndelaforge",
        name = "Nicolas Delaforge",
        email = "nicolas.delaforge@mnemotix.com",
        url = url("http://www.mnemotix.com")
      )
    ),
    GitlabPlugin.autoImport.gitlabDomain :=  "gitlab.com",
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),
    publishTo := Some("gitlab maven" at "https://oss.sonatype.org/content/repositories/snapshots"),
    resolvers ++= Seq(
      Resolver.mavenLocal,
      Resolver.typesafeRepo("releases"),
      Resolver.typesafeIvyRepo("releases"),
      Resolver.sbtPluginRepo("releases"),
      "gitlab-maven" at "https://gitlab.com/api/v4/projects/21727073/packages/maven",
      "gitlab-maven-template-replacer" at "https://gitlab.com/api/v4/projects/35329974/packages/maven"
    ),
    libraryDependencies ++= Dependencies.core,
    Compile / mainClass  := Some("org.ddf.franceTerm.services.JobRunner"),
    run / mainClass := Some("org.ddf.franceTerm.services.JobRunner"),
    assembly / mainClass := Some("org.ddf.franceTerm.services.JobRunner"),
    assembly /assemblyJarName := "france-term-controller.jar",
    imageNames in docker := Seq(
      ImageName(
        namespace = Some("registry.gitlab.com/mnemotix/ddf"),
        repository = artifact.value.name,
        tag = Some(version.value)
      )
    ),
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    docker / buildOptions := BuildOptions(cache = false),
    docker / dockerfile := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("adoptopenjdk/openjdk11:alpine-slim")
        add(artifact, artifactTargetPath)
        run("mkdir", "-p", "/data/")
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  )
