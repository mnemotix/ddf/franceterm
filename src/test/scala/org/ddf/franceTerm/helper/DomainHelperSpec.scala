/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.helper

import akka.actor.ActorSystem
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class DomainHelperSpec extends AnyFlatSpec with Matchers {

  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext = ExecutionContext.global

  val domainHelper = new DomainHelper()

  it should "retrieve domain liste" in {
    val domainHelper = new DomainHelper()
    val fut = domainHelper.domainHelper
    val r = Await.result(fut,Duration.Inf)

    r.foreach(e => println(s"${e.prefLabel} - ${e.uri} - ${e.uri2}"))

    r.size should be > 0
  }
}
