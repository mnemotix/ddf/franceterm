/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.typesafe.scalalogging.LazyLogging
import org.ddf.franceTerm.helper.FranceTermeConverterException

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success}

class FranceTermConverter extends LazyLogging {

  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val materializer: ActorMaterializer =  ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val extractor = new FranceTermExtractor
  val output = new FranceTermCsvOutput()

  def convert = {
    val fut = extractor.extract.via(output.writeAsync).runWith(Sink.ignore)
    fut.onComplete {
      case Success(value) => {
        output.dump
        logger.info(s"the converter finished with the value $value")
      }
      case Failure(exception) => {
        logger.error(s"the converter finished with an error", exception)
        throw FranceTermeConverterException(s"the converter finished with an error", Some(exception))
      }
    }
    fut
  }

}
