/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.helper

import org.ddf.franceTerm.model.Equivalent

object DDFCsv {

  def hasGender(gender: Option[String]) = {
    if (gender.isDefined) {
      if (gender.get.contains("m.")) "http://www.lexinfo.net/ontology/2.0/lexinfo#masculine"
      else if (gender.get.contains("f.")) "http://www.lexinfo.net/ontology/2.0/lexinfo#feminine"
      else ""
    }
    else ""
  }

  def hasPortOfSpeech(pos: Option[String]) = {
    if (pos.isDefined) {
      if (pos.get.contains("adj.")) "http://www.lexinfo.net/ontology/2.0/lexinfo#adjective"
      else if (pos.get.contains("n.")) "http://www.lexinfo.net/ontology/2.0/lexinfo#noun"
      else if (pos.get.contains("v.")) "http://www.lexinfo.net/ontology/2.0/lexinfo#verb"
      else ""
    }
    else ""
  }

  def multiWordType(wr: Option[String]) = {
    if (wr.isDefined) {
      val split1 = wr.get.trim.split(" ").size
      if (split1 > 1 ) "http://data.dictionnairedesfrancophones.org/authority/multiword-type/unspecifiedPhrase" else ""
    }
    else ""
  }

  def termElement(te: Option[String]) = {
    ""
  }

  def otherForm(of: Option[Seq[Equivalent]]) = {
    of.toList.flatten.filter { equivalent =>
      equivalent.equi_prop.isDefined
    }.map(_.equi_prop.get).mkString(";")
  }
}