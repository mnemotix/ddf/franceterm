/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services

import org.ddf.franceTerm.scheduler.CrontabExpressionHelper
import org.ddf.franceTerm.services.job.FranceTermeJob
import org.quartz.{CronScheduleBuilder, JobBuilder, Trigger, TriggerBuilder}
import org.quartz.impl.StdSchedulerFactory

object JobRunner extends App {

  val task = new FranceTermeJob()

  task.run()

  val quartz = StdSchedulerFactory.getDefaultScheduler

  val franceTermeJob= JobBuilder.newJob(classOf[FranceTermeJob]).withIdentity("FranceTermJob", "DDF").build()

  val trigger: Trigger = TriggerBuilder.newTrigger.withIdentity("Trigger", "DDF").withSchedule(CronScheduleBuilder.cronSchedule(CrontabExpressionHelper.EVERY_MONTH_AT_MIDNIGHT)).build
  quartz.start()
}
