/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.mnemotix.synaptix.http.FileDownloader
import org.ddf.franceTerm.helper.FranceTermConfig

import java.io.File
import scala.concurrent.{ExecutionContextExecutor, Future}

class FranceTermDownloader {
  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  lazy val dir = s"${FranceTermConfig.dir}"


  def init = {
      new File(dir).mkdir()
  }

  def download: Future[Done] = {
    val downloader = new FileDownloader
    downloader.downloadFileAsyncToFile(FranceTermConfig.franceTermUrl, FranceTermConfig.franceTermFile).map { ioresult =>
      ioresult.status.get
    }
  }
}