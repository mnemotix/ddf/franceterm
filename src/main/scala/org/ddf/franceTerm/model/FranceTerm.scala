/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.model

case class Domaine(domaine: Option[List[String]], sousDomaine: Option[List[String]])

case class Terme(terme_prop: Option[Seq[String]], categorie: Option[String], note: Option[String], termeBalise: Option[String])

case class Synonyme(status: Option[String], terme: Option[String])

case class Antonyme(status: Option[String], terme: Option[String])

case class Toponyme(status: Option[String], terme: Option[String])

case class Capitale(status: Option[String], terme: Option[String])

case class Equivalent(langue: Option[String], equi_prop: Option[String])

case class Variante(variante_text: Option[String], lang: Option[String], `type`: Option[String], origine: Option[String])

case class Variantes(variante: Seq[Variante], lang: Option[String], `type`: Option[String], origine: Option[String])

case class Voir(text: String)

case class ArticleProperty(numero: Option[String], commission: Option[String], id: Option[String], publie: Option[String], annule: Option[String], administration: Option[String])

case class FranceTerm(acticleProp: ArticleProperty,
                      terme: Terme,
                      synonyme: Seq[Synonyme],
                      antonyme: Seq[Antonyme],
                      toponyme: Seq[Toponyme],
                      capitale: Seq[Capitale],
                      terme_balise: Option[String],
                      terme_exergue: Option[String],
                      datePub: Option[String],
                      note: Option[String],
                      definition: Option[String],
                      variante: Variantes,
                      equivalent: Option[Seq[Equivalent]],
                      voir: Option[Seq[Voir]],
                      domaine: Option[Domaine],
                      lien: Option[Seq[String]],
                      source: Option[String])