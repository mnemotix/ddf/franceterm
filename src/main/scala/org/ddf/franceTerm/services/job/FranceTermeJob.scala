/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services.job

import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.io.FileUtils
import org.ddf.annotator.helpers.DDFDictionnaireConfig
import org.ddf.franceTerm.helper.FranceTermConfig
import org.ddf.franceTerm.services.{FranceTermAnnotator, FranceTermConverter, FranceTermDownloader}
import org.quartz.{Job, JobExecutionContext}

import java.io.File
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.reflect.io.Directory
import scala.util.{Failure, Success}


class FranceTermeJob() extends Job with LazyLogging {

  override def execute(context: JobExecutionContext): Unit = {
    run()
  }

  def run() = {

    implicit val executionContext =  ExecutionContext.global
    val copyLoc = new File(s"${FranceTermConfig.dir}copy/").mkdirs()

    logger.info(s"init")
    beforeRun()

    val d = download()
    d.onComplete {
      case Success(value) => {
        logger.info("download is completed")
      }
      case Failure(exception) => {
        logger.error("error during the download process")
        rollback(s"${FranceTermConfig.dir}copy/")
      }
    }
    Await.result(d, Duration.Inf)

    lazy val c = convert()
    Await.result(c, Duration.Inf)
    c.onComplete {
      case Success(value) => {
        logger.info("xml to csv is completed")
      }
      case Failure(exception) => {
        logger.error("error during the convertion process", exception)
        rollback(s"${FranceTermConfig.dir}copy/")
      }
    }

    annotate()
    //annotateSR()

  }

  def beforeRun() = {
    move(s"${FranceTermConfig.dir}copy/")
    deleteDownload
    deleteCsv
    deleteRdf
  }

  def rollback(copyLoc: String) = {
    val rsDd = com.mnemotix.synaptix.core.utils.FileUtils.copy(s"${copyLoc}franceTerme.xml", s"${FranceTermConfig.franceTermFile}")
    val rsCsv = com.mnemotix.synaptix.core.utils.FileUtils.copy(s"${copyLoc}franceTerme.csv", s"${FranceTermConfig.dir}output/franceTerme.csv")
    val rdf = FileUtils.copyDirectory(new File(s"${copyLoc}rdf/"), new File(DDFDictionnaireConfig.rdfDumpDir))
  }

  def move(copyLoc: String) = {
    if (new File(FranceTermConfig.franceTermFile).exists()) {
      val rsDd = com.mnemotix.synaptix.core.utils.FileUtils.copy(s"${FranceTermConfig.franceTermFile}", s"${copyLoc}franceTerme.xml")
    }
    else logger.info(s"pas de fichiers ${FranceTermConfig.franceTermFile} existant")
    if (new File(s"${FranceTermConfig.dir}output/franceTerme.csv").exists()) {
      val rsCsv = com.mnemotix.synaptix.core.utils.FileUtils.copy(s"${FranceTermConfig.dir}output/franceTerme.csv", s"${copyLoc}franceTerme.csv")
    }
    else logger.info(s"pas de fichiers ${FranceTermConfig.dir}output/franceTerme.csv existant")
    if (new File(DDFDictionnaireConfig.rdfDumpDir).exists()) {
      val rdf = FileUtils.copyDirectory(new File(DDFDictionnaireConfig.rdfDumpDir), new File(s"${copyLoc}rdf/"))
    }
    else logger.info(s"pas de repertoire ${DDFDictionnaireConfig.rdfDumpDir}  existant")
    if (new File(s"${FranceTermConfig.dir}output/franceTermeIssues.csv").exists()) {
      val rsCsv = com.mnemotix.synaptix.core.utils.FileUtils.copy(s"${FranceTermConfig.dir}output/franceTermeIssues.csv", s"${copyLoc}franceTermeIssues.csv")

    }
  }

  def deleteDownload = {
    val del =  FileUtils.deleteQuietly(new File(FranceTermConfig.franceTermFile))
    if (del) logger.info(s"${FranceTermConfig.franceTermFile} is delete")
    else logger.info(s"${FranceTermConfig.franceTermFile} is not delete")
  }

  def deleteCsv = {
    val del =  FileUtils.deleteQuietly(new File(s"${FranceTermConfig.dir}output/franceTerme.csv"))
    if (del) logger.info(s"${FranceTermConfig.dir}output/franceTerme.csv is deleted")
    else logger.info(s"${FranceTermConfig.dir}output/franceTerme.csv is not delete")
  }

  def deleteRdf = {
    val directory = new Directory(new File(DDFDictionnaireConfig.rdfDumpDir))
    val del = directory.deleteRecursively()

    if (del) logger.info(s"${DDFDictionnaireConfig.rdfDumpDir} dir content is deleted")
    else logger.info(s"${DDFDictionnaireConfig.rdfDumpDir} dir content is not delete")
  }

  def download() = {
    val ftdl = new FranceTermDownloader
    ftdl.init
    ftdl.download
  }

  def convert() = {
    val converter = new FranceTermConverter
    converter.convert
  }

  def annotate() = {
    val franceTermAnnotator = new FranceTermAnnotator
    franceTermAnnotator.franceTermAnnotate
  }

  def annotateSR() = {
    val franceTermAnnotator = new FranceTermAnnotator
    franceTermAnnotator.annotateSr
  }
}
