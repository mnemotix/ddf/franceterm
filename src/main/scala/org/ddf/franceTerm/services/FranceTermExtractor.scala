/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services

import akka.NotUsed
import akka.stream.IOResult
import akka.stream.alpakka.xml.scaladsl.XmlParsing
import akka.stream.scaladsl.{FileIO, Flow, Source}
import com.sun.org.apache.xalan.internal.xsltc.trax.DOM2SAX
import com.typesafe.scalalogging.LazyLogging
import org.ddf.franceTerm.helper.{FranceTermConfig, FranceTermModelSerializer, FranceTermeExtractionException}
import org.ddf.franceTerm.model.FranceTerm

import java.nio.file.Paths
import scala.concurrent.Future
import scala.xml.parsing.NoBindingFactoryAdapter

class FranceTermExtractor extends LazyLogging {

  val filePath = FranceTermConfig.franceTermFile

  def extract: Source[FranceTerm, Future[IOResult]] = {
    try {
      FileIO.fromPath(Paths.get(filePath))
        .via(XmlParsing.parser)
        .via(XmlParsing.subtree("CRITER" :: "Article" :: Nil)).map(asXml(_)).via(convert)
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the resource extraction process", t)
        throw FranceTermeExtractionException("An error occured during the resource extraction process", Some(t))
    }
  }

  private def convert: Flow[scala.xml.Node, FranceTerm, NotUsed] = Flow[scala.xml.Node].map[FranceTerm](parse)

  private def parse(elem: scala.xml.Node): FranceTerm = {
    FranceTerm(
      FranceTermModelSerializer.articleProperty(elem),
      FranceTermModelSerializer.terme(elem),
      FranceTermModelSerializer.synonyme(elem),
      FranceTermModelSerializer.antonyme(elem),
      FranceTermModelSerializer.toponyme(elem),
      FranceTermModelSerializer.capitale(elem),
      FranceTermModelSerializer.termeBalise(elem),
      FranceTermModelSerializer.termeExergue(elem),
      FranceTermModelSerializer.datePub(elem),
      FranceTermModelSerializer.note(elem),
      FranceTermModelSerializer.definition(elem),
      FranceTermModelSerializer.variante(elem),
      FranceTermModelSerializer.equivalent(elem),
      FranceTermModelSerializer.voir(elem), // VOIR
      FranceTermModelSerializer.domaine(elem),
      None,
      FranceTermModelSerializer.source(elem)
    )
  }

  private def asXml(dom: org.w3c.dom.Node): scala.xml.Node = {
    val dom2sax = new DOM2SAX(dom)
    val adapter = new NoBindingFactoryAdapter
    dom2sax.setContentHandler(adapter)
    dom2sax.parse()
    adapter.rootElem
  }
}