/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.concurrent.duration.Duration

class FranceTermDownloaderSpec extends AnyFlatSpec with Matchers {

  it should "download franceTerm file" in {
    implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
    implicit val materializer: ActorMaterializer =  ActorMaterializer()
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher

    val ftdl = new FranceTermDownloader
    ftdl.init
    val fut = ftdl.download
    Await.result(fut, Duration.Inf)
  }
}