/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import com.opencsv.CSVWriter
import com.typesafe.scalalogging.LazyLogging
import org.ddf.franceTerm.helper.{DDFCsv, DomainHelper, FranceTermConfig, FranceTermeCsvOutputException}
import org.ddf.franceTerm.model.FranceTerm
import org.ddf.franceTerm.services.helper.FranceTermIssues

import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.JavaConverters.asJavaIterableConverter
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

class FranceTermCsvOutput(implicit val system: ActorSystem, ec: ExecutionContext, materializer: ActorMaterializer) extends LazyLogging {

  val header = Array("ontolex:writtenRep", "skos:definition", "ddf:aboutForm", "prov:wasDerivedFrom",
    "ddf:hasPartOfSpeech", "ddf:multiWordType", "lexinfo:termElement", "ddf:hasItemAboutEtymology", "ontolex:otherForm",
    "ddf:hasGender", "ddf:hasNumber", "ddf:hasTransitivity", "ddf:formHasLocalisation", "lexicog:usageExample",
    "ddf:hasItemAboutSense", "ddf:hasLocalisation",
    "ddf:hasDomain",
    "http://data.dictionnairedesfrancophones.org/authority/sense-relation/synonym", "http://data.dictionnairedesfrancophones.org/authority/sense-relation/antonym", "toponyme", "Capitale",
    "http://data.dictionnairedesfrancophones.org/authority/sense-relation/associativeRelation",
    "http://data.dictionnairedesfrancophones.org/authority/sense-relation/graphicRelation")

  val dirName = FranceTermConfig.dir
  val file = new File(s"${dirName}output/").mkdirs()
  val fileWriter = new FileWriter(new File(s"${dirName}output/franceTerme.csv"), true)
  val outputFile = new BufferedWriter(fileWriter)
  val csvWriter = new CSVWriter(outputFile)

  val fileWriterIssues = new FileWriter(new File(s"${dirName}output/franceTermeIssues.csv"), true)
  val outputFileIssues = new BufferedWriter(fileWriterIssues)
  val csvWriterIssues = new CSVWriter(outputFileIssues)

  val domainHelper = new DomainHelper()
  val futDomainHelper = domainHelper.domainHelper
  val domain = Await.result(futDomainHelper, Duration.Inf)

  lazy val franceTermIssues = new FranceTermIssues()
  val futIssuesProcessed = franceTermIssues.formList
  val listIssuesProcessed = Await.result(futIssuesProcessed, Duration.Inf)

  def writeAsync: Flow[FranceTerm, Unit, NotUsed] = {
    try {
      csvWriter.writeNext(header)
      csvWriterIssues.writeNext(header)
      Flow[FranceTerm].grouped(200).map { fut =>
        val lines = recogsLine(fut)
        val sLines = specialLines(fut)
        val allLines = lines ++ sLines
        csvWriter.writeAll(allLines.toList.asJava)

        val issueslines = issuesLines(fut)
        csvWriterIssues.writeAll(issueslines.toList.asJava)
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the writing of the file", t)
        throw FranceTermeCsvOutputException("An error occured during the writing of the file", Some(t))
    }
  }

  def writeIssues: Flow[FranceTerm, Unit, NotUsed] = {
    try {
      csvWriterIssues.writeNext(header)
      Flow[FranceTerm].grouped(200).map { fut =>
        val lines = issuesLines(fut)
        csvWriterIssues.writeAll(lines.toList.asJava)
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the writing of the file", t)
        throw FranceTermeCsvOutputException("An error occured during the writing of the file", Some(t))
    }
  }

  def recogsLine(terms: Seq[FranceTerm]): Seq[Array[String]] = {
    terms.filter(_.terme.termeBalise.isDefined).filterNot(_.terme.termeBalise.get.contains("Recommandation sur les équivalents français")).filterNot(_.terme.termeBalise.get.contains(",")).map { franceTerm =>
      Array(
        cleaner(franceTerm.terme.termeBalise.getOrElse("")), cleaner(franceTerm.definition.getOrElse("")), "", "",
        cleaner(DDFCsv.hasPortOfSpeech(franceTerm.terme.categorie)), DDFCsv.multiWordType(franceTerm.terme_balise), "", "", "",
        DDFCsv.hasGender(franceTerm.terme.categorie), "", "", "", "",
        franceTerm.note.getOrElse("") + " " + cleaner(franceTerm.source.getOrElse("")), "https://www.geonames.org/3017382",
        domainCol(franceTerm),
        cleaner(franceTerm.synonyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.antonyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.toponyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.capitale.map(_.terme.getOrElse("")).mkString(";")),
        if (franceTerm.voir.isDefined) franceTerm.voir.get.map(_.text).mkString(";") else "",
        if (franceTerm.variante.variante.size > 0) {
          franceTerm.variante.variante.map(_.variante_text.get).mkString(";")
        } else ""
      )
    }
  }

  def specialLines(terms: Seq[FranceTerm]): Seq[Array[String]] = {
    terms.map { franceTerm =>
      val writtenRep = franceTerm.variante.variante.map(_.variante_text.get)
      writtenRep.map { wr =>
        Array(
          wr.trim, cleaner(franceTerm.definition.getOrElse("")), "", "",
          cleaner(DDFCsv.hasPortOfSpeech(franceTerm.terme.categorie)), DDFCsv.multiWordType(franceTerm.terme_balise), "", "", "",
          DDFCsv.hasGender(franceTerm.terme.categorie), "", "", "", "",
          franceTerm.note.getOrElse("") + " " + cleaner(franceTerm.source.getOrElse("")), "https://www.geonames.org/3017382",
          domainCol(franceTerm),
          cleaner(franceTerm.synonyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.antonyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.toponyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.capitale.map(_.terme.getOrElse("")).mkString(";")),
          if (franceTerm.voir.isDefined) cleaner(stringMaker(Some(franceTerm.voir.get.map(_.text).toList))) else "",
          ""
        )
      }
    }.flatten.toSeq
  }

  def issuesLines(terms: Seq[FranceTerm]): Seq[Array[String]] = {
    terms.filter(_.terme.termeBalise.isDefined).filter(_.terme.termeBalise.get.contains(",")).filterNot { t =>
      listIssuesProcessed.contains(t.terme.termeBalise.get)
    }.map { franceTerm =>
      Array(
        cleaner(franceTerm.terme.termeBalise.getOrElse("")), cleaner(franceTerm.definition.getOrElse("")), "", "",
        cleaner(DDFCsv.hasPortOfSpeech(franceTerm.terme.categorie)), DDFCsv.multiWordType(franceTerm.terme_balise), "", "", "",
        DDFCsv.hasGender(franceTerm.terme.categorie), "", "", "", "",
        franceTerm.note.getOrElse("") + " " + cleaner(franceTerm.source.getOrElse("")), "https://www.geonames.org/3017382",
        domainCol(franceTerm),
        cleaner(franceTerm.synonyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.antonyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.toponyme.map(_.terme.getOrElse("")).mkString(";")), cleaner(franceTerm.capitale.map(_.terme.getOrElse("")).mkString(";")),
        if (franceTerm.voir.isDefined) franceTerm.voir.get.map(_.text).mkString(";") else "",
        if (franceTerm.variante.variante.size > 0) {
          franceTerm.variante.variante.map(_.variante_text.get).mkString(";")
        } else ""
      )
    }
  }

  def domainCol(franceTerm: FranceTerm) = {
    val dom = if (franceTerm.domaine.isDefined) domainUri(franceTerm.domaine.get.domaine) else ""
    val sousDom = if (franceTerm.domaine.isDefined) domainUri(franceTerm.domaine.get.sousDomaine) else ""

    val domainCol = {
      if (dom == "") sousDom
      else if (sousDom == "") dom
      else if (dom != "" && sousDom != "") dom + ";" + sousDom
      else ""
    }
    domainCol
  }

  def domainUri(s: Option[List[String]]): String = {
    if (s.isDefined) {
      s.get.filter(_.size > 0).map { dom =>
        val toFilter = dom.trim.toLowerCase
        val lines = domain.filter { d =>
          d.prefLabel == toFilter
        }
        val dm = lines.map(line => Seq(line.uri, line.uri2)).flatten.distinct
        dm
      }.flatten.mkString(";")
    }
    else ""
  }

  def cleanForm(form: String) = {
    if (form.contains(",")) "" else form
  }

  def cleaner(content: String) = {
    content.replaceAll("\n", "").trim
  }

  def stringMaker(s: Option[List[String]]) = {
    if (s.isDefined) cleaner(s.get.mkString(";")) else ""
  }

  def dump: Unit = {
    csvWriter.close()
    fileWriter.close()
    outputFile.close()

    csvWriterIssues.close()
    fileWriterIssues.close()
    outputFileIssues.close()
  }
}