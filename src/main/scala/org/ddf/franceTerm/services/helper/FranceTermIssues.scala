/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.franceTerm.services.helper

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer}
import akka.stream.scaladsl.{Flow, Sink}
import org.ddf.annotator.services.DDFDictionnaireExtractor

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, Future}

class FranceTermIssues(implicit ec: ExecutionContext, system: ActorSystem, materializer: ActorMaterializer) {

  lazy val issuesContribFile = ("src/main/resources/franceTermeIssues - franceTermeIssues.csv")
  /*FranceTermConfig.franceTermIssuesProcessed */

  lazy val extractor = new DDFDictionnaireExtractor(issuesContribFile, Some(','.toByte))

  def formList: Future[immutable.Seq[String]] = {
    val flow: Flow[Map[String, String], String, NotUsed] = Flow[Map[String, String]].map { aMap =>
      val writtenRep = aMap.get("skos:editorialNote (Forme originale problématique)").get.trim
      writtenRep
    }
    extractor.extract.via(flow).runWith(Sink.seq)
  }
}